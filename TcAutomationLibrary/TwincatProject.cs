﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;

using EnvDTE;
using EnvDTE80;
using TCatSysManagerLib;
using TwinCAT.Ads;


namespace TcAutomationLibrary
{
    public class TwincatProject
    {
        //public string TemplateFilePath { get; set; } = @"C:\TwinCAT\3.1\Components\Base\PrjTemplate\TwinCAT Project.tsproj"; //path to project template
        public EnvDTE80.DTE2 ProjectDTE;
        public ITcSysManager14 TcSystemManager;
        public EnvDTE.Solution TcSolution;
        public EnvDTE.Project TcProject;

        public bool SetUpVisualStudio(string visualStudioVersion, bool visible)
        {
            Console.Write("Setup VS...");

            // Create visual studio object
            Type VSType = System.Type.GetTypeFromProgID(visualStudioVersion);

            // Open visual studio version specificed above
            ProjectDTE = (EnvDTE80.DTE2)System.Activator.CreateInstance(VSType);

            ProjectDTE.SuppressUI = true;
            ProjectDTE.MainWindow.Visible = visible;
            ProjectDTE.UserControl = false;

            var settings = ProjectDTE.GetObject("TcAutomationSettings");
            settings.SilentMode = true;

            Console.WriteLine("Done");

            return true;
        }


        /*
        public bool SetupUpDirectory()
        {
            Console.Write("Setting Up Directory Structure...");
            try
            {
                if (Directory.Exists(SolutionPath))
                {
                    Directory.Delete(SolutionPath, true);
                }
                Directory.CreateDirectory(SolutionPath);
                Directory.CreateDirectory(SolutionPath + @"\" + SolutionName);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error seting up directory structure: {0}", e.Message);
                return false;
            }

            Console.WriteLine("Done");
            return true;
        }
        */


        public bool OpenSolution(string solutionPath)    //Open existing TwinCAT solution
        {         
            Console.Write("Opening Solution...");

            try
            {
                if (File.Exists(solutionPath))
                {
                    TcSolution = ProjectDTE.Solution;
                    TcSolution.Open(solutionPath);
                    TcProject = ProjectDTE.Solution.Projects.Item(1);
                    TcSystemManager = TcProject.Object;
                    ITcSmTreeItem ioDevicesItem = TcSystemManager.LookupTreeItem("TIID");
                }
                else
                {
                    Console.WriteLine("Solution file does not exist");
                }
            }
            catch (Exception e)
            {
                //throw new ApplicationException(e.Message);
                Console.WriteLine(e.Message);
                return false;
            }

            Console.WriteLine("Done");

            return true;
        }

        /*
        public bool SetUpProjectFromTemplate(string solutionPath, string solutionName)
        {
            Console.Write("Setting Up project from template...");
            try
            {
                TcSolution = ProjectDTE.Solution;
                TcSolution.Create(solutionPath, solutionName);
                TcSolution.SaveAs(solutionPath + @"\" + solutionName);

                TcProject = TcSolution.AddFromTemplate(TemplateFilePath, solutionPath + @"\" + solutionName, ProjectName);

                TcSystemManager = TcProject.Object;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            Console.WriteLine("Done");

            return true;
        }
        */

        /*
        public void LoadPlcProject(string plcPath)
        {
            Console.Write("Loading Project...");
            ITcSmTreeItem plc = TcSystemManager.LookupTreeItem("TIPC");
            ITcSmTreeItem newProject = plc.CreateChild("MotionPlc", 0, "", plcPath ); // 1 = Move solution 
            Console.WriteLine("Done");
        }
        */


        public void LinkVariables(string plcName, uint axisNum, uint encChannel, uint[] switchInputs, uint switchOutput)
        {
            Console.Write("Linking variables...");

            string source = "TIPC^" + plcName + "^" + plcName + " Instance";
            string destination = "TIID^Device 1 (EtherCAT)";

            // Swtich outputs
            TcSystemManager.LinkVariables(
                source + string.Format("^PlcTask Outputs^MAIN.abSwitchOutputs[{0}]", axisNum), 
                destination + string.Format("^Term 1 (EK1200)^Term 3 (EL2819)^DIG Outputs^Channel {0}^Output", switchOutput));

            // Encoder Outputs
            TcSystemManager.LinkVariables(
                source + string.Format("^PlcTask Outputs^MAIN.abEncoderOutputs[{0}]", axisNum),
                destination + string.Format("^Term 1 (EK1200)^Term 5 (EL2014)^DIG Outputs^Channel {0}^Output", encChannel));

            // Swtich Inputs
            TcSystemManager.LinkVariables(
                source + string.Format("^PlcTask Inputs^GVL.astAxes[{0}].stInputs.bLimitFwd", axisNum),
                destination + string.Format("^Term 1 (EK1200)^Term 2 (EL1808)^Channel {0}^Input", switchInputs[0]));
            TcSystemManager.LinkVariables(
                source + string.Format("^PlcTask Inputs^GVL.astAxes[{0}].stInputs.bLimitBwd", axisNum),
                destination + string.Format("^Term 1 (EK1200)^Term 2 (EL1808)^Channel {0}^Input", switchInputs[1]));
            TcSystemManager.LinkVariables(
                source + string.Format("^PlcTask Inputs^GVL.astAxes[{0}].stInputs.bHomeSensor", axisNum),
                destination + string.Format("^Term 1 (EK1200)^Term 2 (EL1808)^Channel {0}^Input", switchInputs[2]));

            Console.WriteLine("Done");
        }


        /*
        public ITcSmTreeItem CreatePlcProject()
        { 
            OrderScriptContext context = (OrderScriptContext)_context;
            ConfigurationInfo configurationInfo = context.Order.ConfigurationInfo;

            string plcProjectName = configurationInfo.PlcProjectName;
            ITcSmTreeItem plcConfig = SystemManager.LookupTreeItem("TIPC");

            worker.ProgressStatus = string.Format("Creating empty PLC Project '{0}' ...", plcProjectName);
            ITcSmTreeItem plcProjectRoot = plcConfig.CreateChild(plcProjectName, 0, "", vsXaePlcEmptyTemplateName);

            ITcPlcProject plcProjectRootIec = (ITcPlcProject)plcProjectRoot;
            plcProjectRootIec.BootProjectAutostart = true;
            plcProjectRootIec.GenerateBootProject(true);

            ITcSmTreeItem plcProject = plcProjectRoot.LookupChild(plcProjectName + " Project");

            foreach (PlcObjectInfo plcObjectInfo in context.Order.ConfigurationInfo.PlcObjects)
            {
                if (worker.CancellationPending)
                    throw new Exception("Execution cancelled!");

                switch (plcObjectInfo.Type)
                {
                    case PlcObjectType.DataType:
                        createWorksheet((WorksheetInfo)plcObjectInfo, plcProject, worker);
                        break;
                    case PlcObjectType.Library:
                        createLibrary((LibraryInfo)plcObjectInfo, plcProject, worker);
                        break;
                    case PlcObjectType.Placeholder:
                        createPlaceholder((PlaceholderInfo)plcObjectInfo, plcProject, worker);
                        break;
                    case PlcObjectType.POU:
                        createWorksheet((WorksheetInfo)plcObjectInfo, plcProject, worker);
                        break;
                    case PlcObjectType.Itf:
                        createWorksheet((WorksheetInfo)plcObjectInfo, plcProject, worker);
                        break;
                    case PlcObjectType.Gvl:
                        createWorksheet((WorksheetInfo)plcObjectInfo, plcProject, worker);
                        break;
                    default:
                        Debug.Fail("");
                        break;
                }
            }

            ITcSmTreeItem realtimeTasks = systemManager.LookupTreeItem("TIRT");
            ITcSmTreeItem rtTask = realtimeTasks.CreateChild("PlcTask", TreeItemType.Task.AsInt32());

            ITcSmTreeItem taskRef = null;
            worker.ProgressStatus = "Linking PLC instance with task 'PlcTask' ...";

            if (!TryLookupChild(plcProject, "PlcTask", out taskRef))
            {
                if (worker.CancellationPending)
                    throw new Exception("Execution cancelled!");

                taskRef = plcProject.CreateChild("PlcTask", TreeItemType.PlcTask.AsInt32(), "", "MAIN");
            }

            //foreach (ITcSmTreeItem prog in taskRef)
            //{
            //    string name = prog.Name;
            //}

            //ErrorItems errors;

            //if (worker.CancellationPending)
            //    throw new Exception("Execution cancelled!");

            //bool ok = CompileProject(worker, out errors);

            //if (!ok)
            //    throw new ApplicationException(string.Format("Plc Project compile produced '{0}' errors.  Please see Visual Studio Error Window for details.!", plcProject.Name));

            return plcProject;
        }
        */

        public void CreateTask()
        {
            Console.Write("Creating Task...");
            ITcSmTreeItem tasks = TcSystemManager.LookupTreeItem("TIRT");
            tasks.CreateChild("Task 1", 1, null, null);
            Console.WriteLine("Done");
        }

        /*
        public void LinkTask()
        {
            Console.Write("Linking Task...");

            ITcSmTreeItem task1 = TcSystemManager.LookupTreeItem("TIPC^MotionPlc^MotionPlc Project^PlcTask");

            //ITcPlcTaskReference task1 = (ITcPlcTaskReference)SystemManager.LookupTreeItem("TIPC^MotionPlc^MotionPlc Project^PlcTask^MAIN");
            ITcPlcTaskReference task2 = (ITcPlcTaskReference)task1;
            task2.LinkedTask = "TIRT^Task 1"; //NC-Task 1 SVB

            Console.WriteLine("Done");
        }
        */


        /*
        public void AddAxis()
        {
            ITcSmTreeItem motionAxes = TcSystemManager.LookupTreeItem("TINC^NC-Task^Axes");
            motionAxes.ImportChild(@"C:\AxisTemplates.xti", "", true, "Axis 1");
        }
        */

        public void CreateMotion(int numOfAxes, string axisTemplatePath)
        {
            Console.Write("Creating Motion...");

            try { 
                string taskName = "NC-Task 1";
                List<string> AxisNames = new List<string>();
                for (int i = 1; i <= numOfAxes; i++)
                {
                    AxisNames.Add("Axis " + i);
                }

                ITcSmTreeItem ncConfig = TcSystemManager.LookupTreeItem("TINC");
                ITcSmTreeItem task = null;

                if (!TryLookupChild(ncConfig, taskName + " SAF", out task))
                {
                    Console.WriteLine("test 2");
                    task = ncConfig.CreateChild(taskName, 1);
                }
                
                ITcSmTreeItem axes = null;
                TryLookupChild(task, "Axes", out axes);

                foreach (string axisName in AxisNames)
                {
                    ITcSmTreeItem axis = null;

                    if (!TryLookupChild(axes, axisName, out axis))
                    {
                        axis = axes.CreateChild(axisName, 1);
                        ConsumeTemplate(axis, axisTemplatePath + @"\" + axisName + ".xml");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Warning: {0}", ex.Message);
            }

            Console.WriteLine("Done");
        }


        private bool TryLookupChild(ITcSmTreeItem parent, string childName, out ITcSmTreeItem child)
        {
            foreach (ITcSmTreeItem c in parent)
            {
                if (c.Name == childName)
                {
                    child = c;
                    return true;
                }
            }
            child = null;
            return false;
        }


        private void ConsumeTemplate(ITcSmTreeItem item, string templatePath)
        {
            //string templ = Path.Combine(ApplicationDirectory, templatePath);
            string templ = templatePath;

            XmlTextReader reader = new XmlTextReader(templ);
            reader.MoveToContent();
            item.ConsumeXml(reader.ReadOuterXml());
        }


        public void ConsumeMappings(string templatePath, string filename)
        {
            try
            {
                Console.Write("Consuming Mappings...");
                XmlTextReader reader = new XmlTextReader(templatePath + @"\" + filename);
                reader.MoveToContent();
                TcSystemManager.ConsumeMappingInfo(reader.ReadOuterXml());
                Console.WriteLine("Done");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        public void ConsumeMappings(string xmlString)
        {
            try
            {
                Console.Write("Consuming Mappings String...");
                TcSystemManager.ConsumeMappingInfo(xmlString);
                Console.WriteLine("Done");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }



        public void WriteGvl(string plcName, int numOfAxes)
        {
            Console.Write("Writing GVL...");
            ITcSmTreeItem Gvl = TcSystemManager.LookupTreeItem("TIPC^" + plcName + " Project^GVLs^GVL_APP");
            ITcPlcDeclaration GvlDecl = (ITcPlcDeclaration)Gvl;
            GvlDecl.DeclarationText = "VAR_GLOBAL CONSTANT\n    nAXIS_NUM :   UINT:= " + numOfAxes + ";\nEND_VAR";
            Console.WriteLine("Done");
        }


        public void EditVariables(string plcName, int numOfAxes)
        {
            Console.Write("Editing PLC variables...");

            ITcSmTreeItem Pou = TcSystemManager.LookupTreeItem("TIPC^" + plcName + " Project^POUs^MAIN");
            ITcPlcDeclaration PouDecl = (ITcPlcDeclaration)Pou;
            string currentMainDec = PouDecl.DeclarationText;
            string temp = currentMainDec.Replace(
                "bOutput1 AT %Q*: BOOL:= TRUE;",
                "abSwitchOutputs AT %Q*: ARRAY [1..GVL_APP.nAXIS_NUM] OF BOOL:=  [GVL_APP.nAXIS_NUM(TRUE)];" +
                "\n\tabEncoderOutputs AT %Q*: ARRAY[1..GVL_APP.nAXIS_NUM] OF BOOL:=  [GVL_APP.nAXIS_NUM(TRUE)];");
            PouDecl.DeclarationText = temp;

            Console.WriteLine("Done");
        }


        public bool ScanDevicesAndBoxes(string targetNetId)
        {
            Console.Write("Scanning...");

            TcSystemManager.SetTargetNetId(targetNetId);

            System.Threading.Thread.Sleep(5000);
            ITcSmTreeItem ioDevicesItem = TcSystemManager.LookupTreeItem("TIID"); // Get The IO Devices Node

            // Scan Devices (Be sure that the target system is in Config mode!)
            string scannedXml = ioDevicesItem.ProduceXml(); // Produce Xml implicitly starts the ScanDevices on this node.

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(scannedXml); // Loads the Xml data into an XML Document
            XmlNodeList xmlDeviceList = xmlDoc.SelectNodes("TreeItem/DeviceGrpDef/FoundDevices/Device");
            List<ITcSmTreeItem> devices = new List<ITcSmTreeItem>();

            int deviceCount = 0;

            // Add all found devices to configuration
            foreach (XmlNode node in xmlDeviceList)
            {
                // Add a selection or subrange of devices
                int itemSubType = int.Parse(node.SelectSingleNode("ItemSubType").InnerText);
                string typeName = node.SelectSingleNode("ItemSubTypeName").InnerText;
                XmlNode xmlAddress = node.SelectSingleNode("AddressInfo");

                if (itemSubType == 111)
                {
                    ITcSmTreeItem device = ioDevicesItem.CreateChild(string.Format("Device {0} (EtherCAT)", ++deviceCount), itemSubType, string.Empty, null);
                    string xml = string.Format("<TreeItem><DeviceDef>{0}</DeviceDef></TreeItem>", xmlAddress.OuterXml);

                    device.ConsumeXml(xml); // Consume Xml Parameters (here the Address of the Device)
                    devices.Add(device);

                    break; // This shouldn't need to be here, but for some reason my Ethercat master is showing up twice
                }
            }

            // Scan all added devices for attached boxes
            foreach (ITcSmTreeItem device in devices)
            {
                string xml = "<TreeItem><DeviceDef><ScanBoxes>1</ScanBoxes></DeviceDef></TreeItem>"; // Using the "ScanBoxes XML-Method"
                try
                {
                    device.ConsumeXml(xml); // Consume starts the ScanBoxes and inserts every found box/terminal into the configuration
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Warning: {0}", ex.Message);
                }
            }

            Console.WriteLine("Done");

            return true;
        }


        public void SetConfigMode(string amsNetId)
        {
            Console.Write("Setting Config mode...");

            //Create a new instance of class TcAdsClient
            TcAdsClient tcClient = new TcAdsClient();

            try
            {
                // Connect to TwinCAT System Service port 10000
                tcClient.Connect(amsNetId, (int)AmsPort.SystemService);
                // Send desired state
                tcClient.WriteControl(new StateInfo(AdsState.Reconfig, tcClient.ReadState().DeviceState));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
            finally
            {
                tcClient.Dispose();
            }

            Console.WriteLine("Done");
        }


        public void ActivateConfiguration()
        {
            Console.Write("Activating Configuration...");

            try
            {
                TcSystemManager.ActivateConfiguration();
                System.Threading.Thread.Sleep(1000);
                TcSystemManager.StartRestartTwinCAT();
                System.Threading.Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine("Done");
        }


    public Boolean Save()
        {
            Console.Write("Saving...");

            try
            {
                foreach (Project project in this.TcSolution.Projects)
                {
                    project.Save();
                }

                Console.WriteLine("Done");

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return false;
        }

        
        public bool SaveAll()
        {
            try
            {
                ProjectDTE.ExecuteCommand("File.SaveAll");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            return true;
        }
        

        /*
        public Boolean SaveAs()
        {
            Console.Write("Saving As...");
            try
            {
                TcSolution.SaveAs(SolutionPath + @"\" + SolutionName + @"\" + SolutionName + ".sln");
                Console.WriteLine("Done");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return false;
        }
        */

        public void BuildPlcProject(string plcPath)
        {


            Console.Write("Building Project...");
            if (File.Exists(plcPath))
            {
                TcSolution.SolutionBuild.BuildProject("Release|TwinCAT RT (x64)", plcPath, true);
            }
            else
            {
                Console.WriteLine("Project doesn't exist");
            }
            Console.WriteLine("Done");
        }


        public Boolean Build()
        {
            Console.Write("Building...");
            bool buildSucceeded = false;
            try
            {
                ProjectDTE.Solution.SolutionBuild.Build(true);

                vsBuildState state = ProjectDTE.Solution.SolutionBuild.BuildState;

                while (state == vsBuildState.vsBuildStateInProgress)
                {
                    System.Threading.Thread.Sleep(500);
                    state = ProjectDTE.Solution.SolutionBuild.BuildState;
                    Console.WriteLine(state.ToString());
                }

                buildSucceeded = (ProjectDTE.Solution.SolutionBuild.LastBuildInfo == 0 && state == vsBuildState.vsBuildStateDone);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Done");

            return buildSucceeded;
        }


        public void Quit()
        {
            Console.Write("Quitting...");
            ProjectDTE.Quit();
            Console.WriteLine("Done");
        }


        public void DeleteIo()
        {
            Console.Write("Deleting IO...");

            ITcSmTreeItem ioDevicesItem = TcSystemManager.LookupTreeItem("TIID");

            while (ioDevicesItem.ChildCount != 0)
            {
                try
                {
                    ioDevicesItem.DeleteChild(ioDevicesItem.Child[1].Name);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Console.WriteLine("Done");
        }

        public void DeleteAxes()
        {


        }
    }
}
