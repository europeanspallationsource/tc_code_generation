﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Xml;
using DatabaseClasses;
using TcAutomationLibrary;

namespace MotionControl
{
    /// <summary> 
    ///  The MotionProject class takes a PLC object and creates a TwinCAT solution based on the data
    ///  in the PLC object. It doesn't call the TwinCAT Automation Interface functions directly; this 
    ///  is done as the lower layer. It is more to extract the data from the PLC object, sent it
    ///  to the right functions and perform the functions in the right order.
    ///  It also handles all the printouts to the console; this way you could change this layer if
    ///  you prefer a GUI interface.
    /// </summary>
    public class MotionProject
    {
        PLC Plc; // Object that contains data about the PLC and Axes that form input to the project
        TwincatProject plcProject;
        public string SolutionPath { get; set; }
        public string SolutionName { get; set; }
        public string PlcName { get; set; }
        public string VisualStudioVersion { get; set; } = "TcXaeShell.DTE.15.0";

        public MotionProject(PLC plc)
        {
            Plc = plc;
            plcProject = new TwincatProject();
        }

        /// <summary>
        ///  A script that executes all the steps of populating 
        /// </summary>
        public int CreateProject()
        {
            int returnValue = 0;
            // This should be an application/presentation layer
            try
            {
                string plcPath = SolutionPath + @"\" + SolutionName + @"\" + PlcName + @"\" + PlcName + ".plcproj";
                
                Console.WriteLine("Solution Path: {0}", SolutionPath);
                Console.WriteLine("Solution Name: {0}", SolutionName);
                Console.WriteLine("PLC Name: {0}", PlcName);
                Console.WriteLine("PLC Path: {0}", plcPath);
                Console.WriteLine();

                plcProject.SetUpVisualStudio(VisualStudioVersion, false);
                plcProject.OpenSolution(SolutionPath + @"\" + SolutionName + ".sln");
                //plcProject.SetupUpDirectory();
                //plcProject.SetUpProjectFromTemplate();
                plcProject.SetConfigMode(Plc.AmsNetId);
                plcProject.DeleteIo();
                plcProject.ScanDevicesAndBoxes(Plc.AmsNetId);
                //plcProject.CreateTask();
                plcProject.CreateMotion(Plc.AxesList.Count, @"C:\Users\paulbarron\projects\JenkinsWorkspace\tc_code_generation\CrateConfigFiles");
                //plcProject.LoadPlcProject();
                //plcProject.LinkTask();               
                plcProject.WriteGvl(PlcName, Plc.AxesList.Count);
                plcProject.EditVariables(PlcName, Plc.AxesList.Count);
                plcProject.BuildPlcProject(plcPath);
                foreach (Axis axis in Plc.AxesList)
                {
                    plcProject.LinkVariables(
                        PlcName, 
                        axis.AxisNumber, 
                        axis.EncoderChannel, 
                        axis.SwitchInputs, 
                        axis.SwitchOutput);
                }
                //plcProject.ConsumeMappings(@"C:\Users\paulbarron\projects\JenkinsWorkspace\tc_code_generation\CrateConfigFiles", "Mappings.xml");
                GenerateAndConsumeMappingsXml(@"C:\Users\paulbarron\projects\JenkinsWorkspace\tc_code_generation\CrateConfigFiles", "MappingsTemplate.xml");
                plcProject.ActivateConfiguration();
                plcProject.SaveAll();
            }
            catch (Exception e)
            {
                Console.WriteLine("Catching Error! Message: {0}, Source: {1}", e.Message, e.Source);
                returnValue = 1;
            }

            plcProject.Quit();

            return returnValue;
        }

        /// <summary>
        ///  Takes a template for the links between an axis and the IO, fills the details with
        ///  the axis data and consumes the data so the links are added to the project one axis
        ///  at at time.
        /// </summary>
        public void GenerateAndConsumeMappingsXml(string templatePath, string filename)
        {
            try
            {
                Console.Write("Generate and Consume Mappings...");
                XmlTextReader reader = new XmlTextReader(templatePath + @"\" + filename);
                reader.MoveToContent();
                string template = reader.ReadOuterXml();
                foreach (Axis axis in Plc.AxesList)
                {
                    string temp = template;
                    temp = temp.Replace("__AXIS_NAME__", axis.AxisName)
                        .Replace("__AXIS_NUM__", axis.AxisNumber.ToString())
                        .Replace("__MOT_TERMINAL_NUM__", axis.MotorTerminalName)
                        .Replace("__ENC_TERMINAL_NUM__", axis.EncoderTerminalName)
                        .Replace("__CHANNEL_NUM__", axis.EncoderChannel.ToString());
                    plcProject.ConsumeMappings(temp);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("Done");
        }


        public void PrepareForCommit()
        {
            plcProject.DeleteIo();
            plcProject.DeleteAxes();
        }
    }
}