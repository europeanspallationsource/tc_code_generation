﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClasses
{
    public class EncoderModule
    {
        public string ModuleId { get; set; }
        public string ModuleBrand { get; set; }
        public string ModulePartNumber { get; set; }
    }
}
