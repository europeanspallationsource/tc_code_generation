﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Xml;
using System.Runtime.CompilerServices;
using System.Linq;

namespace DatabaseClasses
{
    public abstract class Database
    {
        public string Filename { get; set; }
        public uint NumberOfInstruments { get; private set; }
        public uint NumberOfPlcs 
        {
            get 
            {
                uint plcCount = 0;
                foreach (Instrument inst in InstrumentList)
                {
                    plcCount += inst.NumberOfPlcs;
                }
                return plcCount;
            }  
        }
        public uint NumberOfAxes
        {
            get
            {
                uint axisCount = 0;
                foreach (Instrument inst in InstrumentList)
                {
                    foreach (PLC plc in inst.PlcList)
                    {
                        axisCount += plc.NumberOfAxes;
                    }
                }
                return axisCount;
            }
        }
        public List<Instrument> InstrumentList;
        public List<Motor> MotorList; // Stores all available motors
        public List<MotorDrive> MotorDriveList; // Stores all available motors
        public List<Encoder> EncoderList; // Stores all available encoders
        public List<EncoderModule> EncoderModuleList; // Stores all available encoders
        public Database()
        {
            InstrumentList = new List<Instrument>();
            MotorList = new List<Motor>();
            MotorDriveList = new List<MotorDrive>();
            EncoderList = new List<Encoder>();
            EncoderModuleList = new List<EncoderModule>();
            NumberOfInstruments = 0;
        }
        public void AddInstrument(Instrument instrument)
        {
            this.InstrumentList.Add(instrument);
            NumberOfInstruments += 1;
        }
        public abstract int LoadPlcs();
        public abstract int LoadAxes();
        public abstract int LoadMotors();
        public abstract int LoadMotorDrives();
        public abstract int LoadEncoders();
        public abstract int LoadEncoderModules();
        public virtual int AddPlc(string instrumentId, uint plcNum) { return 0;  }
        public virtual int AddAxis(string instrumentId, uint plcId, uint axisId, string deviceName, string axisName) { return 0; }
        public void AddMotor(Motor motor)
        {
            MotorList.Add(motor);
        }
        public void AddEncoder(Encoder encoder)
        {
            EncoderList.Add(encoder);
        }
        public virtual int DeletePlc(string instrumentName, uint plcId) 
        {
            Instrument instrument = InstrumentList.Single(inst => inst.Name == instrumentName);
            instrument.DeletePlc(plcId);

            return 0;
        }

        public virtual int DeleteAxis(string instrumentName, uint plcId, uint axisId)
        {
            Instrument instrument = InstrumentList.Single(inst => inst.Name == instrumentName);
            PLC plc = instrument.PlcList.Single(p => p.PlcId == plcId);
            plc.DeleteAxis(axisId);

            return 0;
        }
        public abstract bool EditPlc();
        public abstract bool EditAxis();
        public abstract PLC SearchPlc();
        public abstract Axis SearchAxis();
        public override String ToString()
        {
            string returnString = "";
            foreach (var instrument in InstrumentList)
            {
                returnString += instrument.ToString();
            }

            return returnString;
        }
    }
}
