﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClasses
{
    public class Encoder
    {
        public uint EncoderId { get; set; }
        public string EncoderBrand { get; set; }
        public string EncoderPartNumber { get; set; }
        public string MeasurementType { get; set; } // Incremental or Absolute
        public string MeasurementTechnique { get; set; } // Linear or rotary
        public string SensorTechnology { get; set; } // Magnetic, Optical, Inductive, Capacitive, Laser
        public string Interface { get; set; } // SSI
        public uint SingleTurnBits { get; set; }
        public uint MultiTurnBits { get; set; } // Set to 1 if single turn
        public int MechanicsRatio { get; set; } // Set to 1 if no conversion
    }
}
