﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClasses
{
    public class MotorDrive
    {
        public string DriveId { get; set; }
        public string DriveBrand { get; set; }
        public string DrivePartNumber { get; set; }
        public string DriveType { get; set; }
    }
}
