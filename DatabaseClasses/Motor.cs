﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseClasses
{
    public class Motor
    {
        public uint MotorId { get; set; }
        public string MotorBrand { get; set; }
        public string MotorPartNumber { get; set; }
        public string MotorType { get; set; }
        public uint Steps { get; set; } = 200;
        public uint Voltage { get; set; } = 48;
        public uint Current { get; set; } = 1;
        public int GearboxRatio { get; set; } = 1;
        public int MechanicsRatio { get; set; } = 1;
    }
}
