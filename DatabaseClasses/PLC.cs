﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace DatabaseClasses
{
    public class PLC
    {
        static public uint TotalFacilityPlcs { get; set; } = 0;
        public uint PlcId { get; set; }
        public uint PlcNum { get; set; }
        public uint NumberOfAxes { get { return (uint)AxesList.Count; } }
        public List<Axis> AxesList;
        public string IpAdress { get; set; }
        public string AmsNetId { get; set; }

        public PLC(uint plcId) // Constructor
        {
            PlcId = plcId;
            AxesList = new List<Axis>();
            TotalFacilityPlcs++;
        }
        ~PLC()
        {
            TotalFacilityPlcs--;
        }

        public void AddAxis(
            uint axisID,
            uint axisNum,
            string deviceName,
            string axisName,
            uint motorId,
            string motorDriveId,
            uint encoderId,
            string encoderModuleId,
            string motorTerminalName,
            string encoderTerminalName,
            uint encoderChannel,
            uint[] switchInputs,
            uint switchOutput)
        {
            AxesList.Add(new Axis(NumberOfAxes + 1)
            {
                AxisID = axisID,
                AxisNumber = axisNum,
                DeviceName = deviceName,
                AxisName = axisName,
                EncoderChannel = encoderChannel,
                MotorTerminalName = motorTerminalName,
                EncoderTerminalName = encoderTerminalName,
                SwitchInputs = switchInputs,
                SwitchOutput = switchOutput,
                AxisMotor = new Motor()
                {
                    MotorId = motorId
                    //MotorBrand = motBrand,
                    //MotorPartNumber = motPartNo,
                },
                AxisEncoder = new Encoder()
                {
                    EncoderId = encoderId
                    //EncoderBrand = encBrand,
                    //EncoderPartNumber = encPartNo
                },
                AxisMotorDrive = new MotorDrive()
                {
                    DriveId = motorDriveId
                },
                AxisEncoderModule = new EncoderModule()
                {
                    ModuleId = encoderModuleId
                }
            });
        }

        public bool DeleteAxis(uint axisId)
        {
            if (AxesList.Remove(AxesList.Single(axis => axis.AxisID == axisId)))
            {
                return true;
            }

            return false;
        }


        public void EmptyAxisList()
        {
            AxesList.Clear();
        }

        public override String ToString()
        {
            string returnString = "CPU: " + PlcId + Environment.NewLine;
            foreach (var axis in AxesList)
            {
                returnString += axis.ToString();
            }
            return returnString;
        }
    }
}
