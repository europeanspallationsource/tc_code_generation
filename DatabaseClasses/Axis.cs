﻿using System;
using System.Runtime.Serialization;

namespace DatabaseClasses
{
    /// <summary>
    ///  This class reprsents an axis object. It 
    /// </summary>
    public class Axis
    {
        static public uint TotalFacilityAxes { get; set; }
        public uint AxisID { get; set; }
        public uint AxisNumber { get; set; }
        public string DeviceName { get; set; }
        public string AxisName { get; set; }
        public string MotionType { get; set; } // Lienar or rotary
        public Motor AxisMotor { get; set; }
        public Encoder AxisEncoder { get; set; }
        public MotorDrive AxisMotorDrive { get; set; }
        public EncoderModule AxisEncoderModule { get; set; }
        public string MotorTerminalName { get; set; }
        public string EncoderTerminalName { get; set; }
        public uint EncoderChannel { get; set; }
        public uint[] SwitchInputs { get; set; }
        public uint SwitchOutput { get; set; }
        //public uint Range { get; set; } // Softlimits
        //public uint DefaultVelocity { get; set; }
        //public uint DefaultAcceleration { get; set; }
        public Axis(uint axisNum) // Constructor
        {
            TotalFacilityAxes++;
            AxisNumber = axisNum;
        }
        ~Axis()
        {
            TotalFacilityAxes--;
        }

        public override String ToString()
        {
            String returnString = "  " + AxisNumber.ToString() + ": " +
                DeviceName + " - " +
                AxisName + Environment.NewLine +
                "    Motor: " + AxisMotor.MotorBrand + Environment.NewLine +
                "    Encoder: " + AxisEncoder.EncoderBrand + Environment.NewLine;
            return returnString;
        }
    }
}