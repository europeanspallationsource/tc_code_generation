﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Schema;

namespace DatabaseClasses
{
    public class Instrument
    {
        static public uint TotalInstruments { get; set; } = 0;

        //public InstrumentNames Name { get; protected set; }
        public string Name;
        public uint NumberOfPlcs { get { return (uint)PlcList.Count; } }
        public uint NumberOfAxes 
        {
            get 
            {
                uint totalAxes = 0;
                foreach (PLC plc in PlcList )
                {
                    totalAxes += plc.NumberOfAxes;
                }
                return totalAxes;
            }
        }
        public List<PLC> PlcList { get; private set; }
        //public Instrument(InstrumentNames name)
        public Instrument(string name)
        {
            Name = name;
            PlcList = new List<PLC>();
            TotalInstruments += 1;
        }

        public bool AddPlc()
        {
            PlcList.Add(new PLC(NumberOfPlcs+1) );
            return true;
        }

        public void DeletePlc(uint plcId)
        {
            PlcList.RemoveAll(plc => plc.PlcId == plcId);
        }

        public void EmptyPlcList()
        {
            PlcList.Clear();
        }

        public override String ToString()
        {
            string returnString = Name + " Axis List" + Environment.NewLine;
            foreach (var plc in PlcList)
            {
                returnString += plc.ToString();
            }
            return returnString;
        }
    }
    public enum InstrumentNames
    {
    BEER = 1, BIFROST = 2, CSPEC = 3, DREAM = 4,
    ESTIA = 5, FREIA = 6, HEIMDAL = 7, LOKI = 8, 
    MAGIC = 9, MIRACLES = 10, NMX = 11, ODIN = 12, 
    SKADI = 13, TREX = 14, VESPA =15 , TESTBEAMLINE = 16,
    MCU025 = 17
    }
}
