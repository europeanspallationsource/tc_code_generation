﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MotionControl;
using DatabaseClasses;

namespace CodeGeneration
{
    class Program
    {
        static int Main()
        {
            Console.WriteLine("Script Application Starting...");

            PLC plc = new PLC(1) { AmsNetId = "5.65.73.242.1.1" };
            plc.AddAxis(1, 1, "Test Crate", "Axis 1", 1, "EL7041", 1, "EL5002", "6", "4", 1, new uint[] { 1, 2, 3 }, 1);
            plc.AddAxis(2, 2, "Test Crate", "Axis 2", 1, "EL7041", 1, "EL5002", "7", "4", 2, new uint[] { 6, 5, 7 }, 5);

            MotionProject twincatProject = new MotionProject(plc)
            {
                SolutionPath = @"C:\Users\paulbarron\projects\JenkinsWorkspace\tc_generic_structure",
                SolutionName = @"solution",
                PlcName = @"tc_project_app"
            };

            int returnValue = twincatProject.CreateProject();

            Console.WriteLine("\n...Script Application Finished");

            return returnValue;
        }
    }
}
